'use strict';


app.directive("uldurbg", function() {

  return {
    restrict: "A",
    link: function(scope, element, attrs) {

      var ctx = uldurbg.getContext('2d');

      ctx.clearRect(0, 0, uldurbg.width, uldurbg.height);

      var colors = ["#222", "#D3D3D3"], 
          x = 100, 
          y = 50,
          flip = 0;

      for (var i=0;i<21;i++) {
        ctx.fillStyle = colors[flip];
        ctx.fillRect(x,y,100,100);
        if ((i == 6) || (i == 13)) {x = 100; y += 100} else {x += 100;}      
        flip = 1 - flip;
      }
    }
  };

});

app.directive("uldur", function(Game) {

  var player_unit = new Image();
  var enemy_unit = new Image();
  var player_cell;
  var enemy_cell; 
  var vision = false; // this determines whether the enemy unit will be visible or not
  var web = new Image();
  var web_x = 0;
  var web_y = 50;
  var attacking = false; // Attack goes to true if there is an attack in progress
  var finished = false; // Use to indicate that game is finished
  var scope; // scope of the the game
  var web_default_x = 0;
  var web_default_y = 50;
  var text;

  return {
    restrict: "A",
    link: function(gamescope, element, attrs) {
      scope = gamescope; // put the game scope in the global variable
      
      var ctx = uldur.getContext('2d');

      // Draw the player and enemy units in initial locations
      player_unit.src = "images/uldur/spider.png";
      player_unit.onload = function() {
        ctx.drawImage(player_unit, 0, 150);
      };
      enemy_unit.src = "images/uldur/spider_enemy.png"; 
      enemy_unit.onload = function() {
        ctx.drawImage(enemy_unit, 800, 150);
      };
      // Draw the attack web
      web.src = "images/uldur/web.png";
      web.onload;

      init(ctx);

      uldur.addEventListener('mousedown', function (ev) {
        var x = ev.clientX - 150;
        var y = ev.clientY - 80;
        
        // Check if the web was clicked  
        if (finished == true) {
          init(ctx);
          finished = false;
        } else if (player_cell[0] == "A" && y < 150 && x < 100) {
          attacking = true;
        } else if (attacking == false) {
          // process the player turn
          if (playerturn (ctx, x, y)) {
            aiturn(ctx); // let the AI make a move
            // Update the engine
            update(ctx);
            winner(ctx); // check if there is a winner or not
          } 
        }

      }, false);

      uldur.addEventListener('mousemove', function (ev) {
        if (attacking == true) {
          web_x = ev.clientX - 200;
          web_y = ev.clientY - 125;
          update(ctx);
        }
        
      }, false);

      uldur.addEventListener('mouseup', function (ev) {
        // If player is currently attacking, the release should indicate the attacked position
        // If attack fails, player loses, if attack successful player wins
        if (attacking == true) {
          // reset web location and flag
          web_x = web_default_x;
          web_y = web_default_y;
          attacking = false;
          var x = ev.clientX - 150;
          var y = ev.clientY - 80;
          // Check if attack is in a valid cell
          if (y > 50 && y < 350 && x > 100 && x < 800) {
            // Get the cell name where attack is performed
            var attack_cell = findcell(x, y);
            // If the enemy is in the attacked cell, player wins
            if (attack_cell == enemy_cell) {
              // Player wins
              playerwins(ctx);
            } else {
              // Enemy wins
              enemywins(ctx);
            }
          } else {
            web_x = web_default_x;
            web_y = web_default_y;
            attacking = false;
            update(ctx);
          }

        }
      }, false);      
    }
  };

  // Update runs every time something changes
  function update(ctx) {
    // Clear everything
    ctx.clearRect(0, 0, uldur.width, uldur.height);
    // If both players are in the same cell, end as a tie
    if (enemy_cell == player_cell) {
      tie(ctx);
    } else {
      // Draw the player unit
      var player_location = getlocation(player_cell);
      ctx.drawImage(player_unit, player_location.x, player_location.y);
      // Draw the enemy unit
      var enemy_location = getlocation(enemy_cell);
      if (vision == true) ctx.drawImage(enemy_unit, enemy_location.x, enemy_location.y); // only draw enemy unit if enemy is visible
      // Draw the Web
      if (player_cell[0] == "A") {ctx.drawImage(web, web_x, web_y)};
      // Finally place an instruction to help player
      place_instruction(ctx);
    }
  }

  // Places a help instruction
  function place_instruction(ctx) {
    if (player_cell[0] == "A") {
      text = "Attack Lane! To attack drop the web on the enemy";
    } else if (player_cell[0] == "M") { 
      text = "Move Lane! You can move two at time";
    } else if (player_cell[0] == "R") {
      text = "Reveal Lane! You can see the location of the enemy";
    }
    instruction(ctx);
  }

  // Checks if there is a winner in the game yet, if there is a winner display winning message, process payout and restart game
  function winner(ctx, W, H) {
    if (player_cell[1] == 8) {
      playerwins(ctx);
    } else if (enemy_cell[1] == 0) {
      enemywins(ctx);
    }
  }

  function tie(ctx) {
    ctx.clearRect(0, 0, uldur.width, uldur.height);
    finished = true;
    text = "Tie! You crashed into each other! Click anywhere to start a new game!";
    instruction(ctx);
  }  

  function playerwins(ctx) {
    ctx.clearRect(0, 0, uldur.width, uldur.height);
    Game.win(scope);
    finished = true;
    text = "You Win! Click anywhere to start a new game!";
    instruction(ctx);
    // Reveal the enemy and player location
    var enemy_location = getlocation(enemy_cell);
    ctx.drawImage(enemy_unit, enemy_location.x, enemy_location.y);
  }

  function enemywins(ctx) {
    ctx.clearRect(0, 0, uldur.width, uldur.height);
    Game.loss(scope);
    finished = true;
    text = "You Lose! Click anywhere to start a new game!";
    instruction(ctx);
    // Reveal the enemy location
    var enemy_location = getlocation(enemy_cell);
    ctx.drawImage(enemy_unit, enemy_location.x, enemy_location.y);
  }

  // AI turn
  function aiturn(ctx) {
    // For now make a stupid AI that picks a random target cell
    var target = "";
    var rows = ["A", "M", "R"];
    var random_row = rows[Math.floor(Math.random() * 3) + 0]
    target += random_row;
    target += parseInt(enemy_cell[1])-1;
    enemy_cell = target;
  }

  // Player turn
  function playerturn(ctx, x, y) {

    // based on what user clicked, get the cell
    var target = findcell(x, y);

    // Check if possible to move to target cell
    if (checkmove(target)) {
      // If target cell is in Reveal row -- Bottom row, flip vision to true, else false
      if (target[0] == "R") {
        vision = true;
      } else {
        vision = false;
      }    
      // set current location
      player_cell = target;

      return true;
    }

    return false;
  }

  // Check if move is possible by player
  function checkmove(target) {
    var step_difference = parseInt(target[1]) - parseInt(player_cell[1]);
    // Player cannot go backwards
    if (target[1] <= player_cell[1]) {
      return false;
    }
    // Player cannot move more than 1 cells unless in mid row
    if (step_difference > 1) {
      if (player_cell[0] != "M" || target[0] != "M") return false;
    // player cannot move 2 rows at once
    }
    if ((player_cell[0] == "A" && target[0] == "R") || (player_cell[0] == "R" && target[0] == "A")) {
      return false;
    }
    // Move is possible
    return true;
  }

  // Clear a unit cell
  function clearcell(ctx, cell) {
    // Check if Unit is in default position
    if (cell[0] == "P") {
      ctx.clearRect(0, 150, 95, 95);
    } else if (cell[0] == "E") {
      ctx.clearRect(800, 150, 95, 95);
    } else {
      var location = getlocation(cell);
      if (cell[0] == "A") {
        ctx.fillStyle = A;
      } else if (cell[0] == "M") {
        ctx.fillStyle = M;
      } else {
        ctx.fillStyle = R;
      }
      ctx.fillRect(location.x+5, location.y+5, 90, 90);
    }
  }

  // Init the game engine
  function init(ctx) {
    // clear the canvas
    ctx.clearRect(0, 0, uldur.width, uldur.height);

    // Place the start message text
    text = "Place your spider in a lane to start";
    instruction(ctx);

    // Init the location of the unites
    enemy_cell = "E8"; 
    player_cell = "P0";

    // Draw the units in initial position
    ctx.drawImage(player_unit, 0, 150);
    ctx.drawImage(enemy_unit, 800, 150);

  }  

  // Place an instructions text
  function instruction(ctx) {
    ctx.fillStyle = "#000";
    ctx.font="20px Georgia";
    ctx.textAlign="center";
    ctx.fillText(text,450,20);
  }

  // Find the exact cell where the unit will go
  // Don't do anything if cell location isn't permitted 
  function findcell(x, y) {
    // Get the current line, line A is for Attack, line M is for move and line R is for reveal
    var cell = "";
    if (y < 150) {
      cell += "A"
    } else if (y < 250) {
      cell += "M"
    } else {
      cell += "R"
    }
    // Get the column number, number goes from 1 to 7, arriving to 7 is winning
    cell += ('' + x)[0]
    return cell;
  }

  // Returns a cell location
  function getlocation(cell) {
    var x, y;
    if (cell[0] == "A") {
      y = 50;
    } else if (cell[0] == "M") {
      y = 150;
    } else if (cell[0] == "R") {
      y = 250;
    }
    x = parseInt(cell[1]+"00");
    return {x: x, y: y}
  }

  // Draw the lines that separate each row
  function drawlines(ctx, x, y, type) {
    if (type == 0) {
      ctx.fillStyle = A;
    } else if (type == 1) {
      ctx.fillStyle = M;
    } else {
      ctx.fillStyle = R;
    }
    ctx.fillRect(x,y,700,100);
  }

});
