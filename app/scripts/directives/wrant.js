'use strict';

// the position of each wall, respectively x1,x2,y1,y2
// x1 where wall starts and x2 where wall ends in the x axis
// y1 where wall stats and x2 where wall ends in y axis
var walls = [[300,350,0,80]], 
    top_margin = 105,
    left_margin = 175,
    finished = false,
    soldier_sprite = new Image(),
    ctx,
    scope,
    velocity = 5,
    W = 800,
    H = 400;


var soldier = {
  h : 50,
  w : 50,
  x : 0,
  y : 0,
  vx: 5,
  vy: 0,
}


app.directive("wrantbg", function() {

  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      var ctx = wrantbg.getContext('2d');
      ctx.clearRect(0, 0, wrantbg.width, wrantbg.height);

      // draw the walls
      var wall = new Image();
      wall.src = "images/wrant/wall.png";
      wall.onload = function() {
        for (var i=0;i<walls.length;i++) {
          ctx.drawImage(wall, walls[i][0], walls[i][2]);
        }
      };
    }
  };
});

app.directive("wrant", function(Game) {

  return {
    restrict: "A",
    link: function(gamescope, element, attrs) {
      scope = gamescope; // put the game scope in the global variable
      ctx = wrant.getContext('2d');
      init(ctx);

      window.requestAnimFrame = (function(){
        return  window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                function( callback ){
                  window.setTimeout(callback, 1000 / 60);
                };
      })();

      (function animloop(){
        requestAnimFrame(animloop);
        update();
      })();

      // Load soldier_sprite unit
      soldier_sprite.src = "images/wrant/soldier.png";
      soldier_sprite.onload;

      wrant.addEventListener('mousedown', function (ev) {
        var x = ev.clientX - left_margin;
        var y = ev.clientY - top_margin;
        
        if (finished == true) {
          init();
          finished = false;
        } else {
          soldier.x = x;
          soldier.y = y;
        }

      }, false);   
    }
  };

  // Update function
  function update() {
    ctx.clearRect(0, 0, wrant.width, wrant.height);
    ctx.drawImage(soldier_sprite, soldier.x, soldier.y);

    // Look for collision with any limits of the canvas
    if (soldier.x < 0) {
      soldier.vx = velocity; soldier.vy = 0;
    } else if (soldier.y < 0) {
      soldier.vx += 0; soldier.vy = velocity; 
    } else if (soldier.y > H ) {
      soldier.vx = velocity; soldier.vy = -velocity;
    } else if (soldier.x > W) {
      soldier.vx = -velocity; soldier.vy = 0;
    } 

    // Make the unit move to the target direction with the target velocity
    soldier.x += soldier.vx;
    soldier.y += soldier.vy;
    collision();
  }


  // Checks if there is a collision between soldier unit and wall
  function collision() {
    for (var i=0;i<walls.length;i++) {
      // detect left collision
      var x1 = soldier.x, x2 = soldier.x+soldier.w,
          y1 = soldier.y, y2 = soldier.y+soldier.h;
      if (x2 >= walls[i][0] && x2 <= walls[i][0] + velocity) {
        change_direction();
        return true;
      }
    };
    // No collision detected
    return false;
  }

  function change_direction() {
    // First of all, check if the unit is in a corner
    //if (soldier.x <= velocity)
    // If unit is moving in the x axis, make it move on the y axis on either direction 
    if (soldier.vx != 0) {
      console.log("changing x");
      soldier.vx = 0;
      soldier.x -= velocity; // needed to get away of the wall
      if (Math.round(Math.random()) == 1) {
        soldier.vy = velocity;
      } else {
        soldier.vy = -velocity;
      }
    } else if (soldier.vy != 0) {
      console.log("changing y");
      soldier.vy = 0;
      soldier.y -= velocity; // needed to get away of the wall
      if (Math.round(Math.random()) == 1) {
        soldier.vx = velocity;
      } else {
        soldier.vx = -velocity;
      }
    }
  }

  function tie() {
    ctx.clearRect(0, 0, wrant.width, wrant.height);
    finished = true;
    text = "Tie! Click anywhere to start a new game!";
    instruction(ctx);
  }  

  function playerwins() {
    ctx.clearRect(0, 0, wrant.width, wrant.height);
    Game.win(scope);
    finished = true;
    text = "You Win! Click anywhere to start a new game!";
    instruction(ctx);
  }

  function enemywins() {
    ctx.clearRect(0, 0, wrant.width, wrant.height);
    Game.loses(scope);
    finished = true;
    text = "You Lose! Click anywhere to start a new game!";
    instruction(ctx);
  }

  // Init the game engine
  function init() {
    // clear the canvas
    ctx.clearRect(0, 0, wrant.width, wrant.height);

  }  

  // Place an instructions text
  function instruction() {
    ctx.fillStyle = "#000";
    ctx.font="20px Georgia";
    ctx.textAlign="center";
    ctx.fillText(text,450,20);
  }


});
