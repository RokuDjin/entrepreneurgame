'use strict';

app.directive("player", function(){

  return {
    restrict: "A",
    scope: {
        'player' : '=',
        'username': '='
    },
    link: function(scope, element, attrs) {

      var canvas = element[0];
      var ctx = canvas.getContext('2d');
      ctx.fillStyle = "black";
      var H = canvas.height;
      var W = canvas.width;

      drawavatar(ctx, scope.player);

      // Draw the username below the avatar
      scope.$watch('player.username', function (newVal) {
        if (scope.username && newVal != undefined) {
          ctx.clearRect(0,110,150,30);
          text(ctx, newVal, W/2, 120);
        }
      });

      // watch if the avatar body changes and then redraw the avatar
      scope.$watch('player.body', function (newVal, oldVal, scope) {
        drawavatar(ctx, scope.player);
      });

      // watch if the avatar head changes and then redraw the avatar
      scope.$watch('player.head', function (newVal, oldVal, scope) {
        drawavatar(ctx, scope.player);
      });

      // watch if the avatar hair changes and then redraw the avatar
      scope.$watch('player.hair', function (newVal, oldVal, scope) {
        drawavatar(ctx, scope.player);
      });

      canvas.addEventListener('mousedown', function (ev) {
        window.location.href = "/#/player";
      });

    }
  };

  function drawavatar(ctx, player) {
    if (player != undefined) {
      // Clear old Avatar
      ctx.clearRect(0,0,canvas.width,canvas.height);
      // Draw the player head, body and hair
      draw(ctx, player);
    }
  }

  function draw(ctx, player) {
  var head = new Image(),
      body = new Image(),
      hair = new Image();
    head.src = player.head;
    body.src = player.body;
    hair.src = player.hair;
    head.onload = function() {
      ctx.drawImage(head, 18, 0);
      body.onload = function() {
        ctx.drawImage(body, 18, 0);
        hair.onload = function() {
          ctx.drawImage(hair, 18, 0);
        }
      }
    };
  }

  function text(ctx, text, x, y) {
    ctx.font = "16px Arial, sans-serif";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillStlye = "white";
    ctx.fillText(text, x, y );
  }
});
