'use strict';

app.directive("universebg", function() {

  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      var ctx = universebg.getContext('2d');
      ctx.clearRect(0, 0, universebg.width, universebg.height);
    }
  };

});

app.directive("universe", function() {

  var top_margin = 130,
      left_margin = 200;

  return {
    restrict: "A",
    link: function(scope, element, attrs) {

      var canvas = element[0];
      var ctx = canvas.getContext('2d');
      ctx.fillStyle = "black";

      // Drawing Uldur
      var uldur = new Image();
      uldur.src = "images/uldur/uldur.png";
      uldur.onload = function() {
        ctx.drawImage(uldur, 700, 50);
      };

      canvas.addEventListener('mousedown', function (ev) {
        var x = ev.x - left_margin,
            y = ev.y - top_margin;
        console.log(x + " " + y);
        if (x > 550 && x < 600 && y > 240 && y < 340) {
          window.location.href = "/#/player"; // Player page
        } else if (x > 700 && x < 800 && y > 50 && y < 100) {
          window.location.href = "/#/uldur"; // Player page
        }
      });
    }
  };

});
