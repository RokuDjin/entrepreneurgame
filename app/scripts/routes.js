"use strict";

app.constant('ROUTES', {
    '/universe': {
      templateUrl: 'views/universe.html',
      controller: 'UniverseCtrl',
      authRequired: true
    },
    '/': {
      templateUrl: 'views/visitor.html',
      controller: 'AuthCtrl'
    },
    '/player': {
      templateUrl: 'views/player.html',
      controller: 'PlayerCtrl',
      authRequired: true
    },
    '/bank': {
      templateUrl: 'views/bank.html',
      controller: 'BankCtrl',
      authRequired: true
    },
      '/shopping': {
      templateUrl: 'views/shopping.html',
      controller: 'ShoppingCtrl',
      authRequired: true
    },
      '/home': {
      templateUrl: 'views/home.html',
      controller: 'HomeCtrl',
      authRequired: true
    },
    '/uldur': {
      templateUrl: 'views/uldur.html',
      controller: 'UldurCtrl',
      authRequired: true
    },
    '/wrant': {
      templateUrl: 'views/wrant.html',
      controller: 'WrantCtrl',
      authRequired: true
    },
  })

  /**
   * Adds a special `whenAuthenticated` method onto $routeProvider. This special method,
   * when called, invokes the requireUser() service (see simpleLogin.js).
   *
   * The promise either resolves to the authenticated user object and makes it available to
   * dependency injection (see AuthCtrl), or rejects the promise if user is not logged in,
   * forcing a redirect to the /login page
   */
  .config(function($routeProvider) {
    // credits for this idea: https://groups.google.com/forum/#!msg/angular/dPr9BpIZID0/MgWVluo_Tg8J
    // unfortunately, a decorator cannot be use here because they are not applied until after
    // the .config calls resolve, so they can't be used during route configuration, so we have
    // to hack it directly onto the $routeProvider object
    $routeProvider.whenAuthenticated = function(path, route) {
      route.resolve = route.resolve || {};
      route.resolve.user = ['requireUser', function(requireUser) {
        return requireUser();
      }];
      $routeProvider.when(path, route);
    }
  })

  // configure views; the authRequired parameter is used for specifying pages
  // which should only be available while logged in
  .config(['$routeProvider', 'ROUTES', function($routeProvider, ROUTES) {
    angular.forEach(ROUTES, function(route, path) {
      if( route.authRequired ) {
        // adds a {resolve: user: {...}} promise which is rejected if
        // the user is not authenticated or fulfills with the user object
        // on success (the user object is then available to dependency injection)
        $routeProvider.whenAuthenticated(path, route);
      }
      else {
        // all other routes are added normally
        $routeProvider.when(path, route);
      }
    });
    // routes which are not in our map are redirected to /home
    $routeProvider.otherwise({redirectTo: '/home'});
  }])

  /**
   * Apply some route security. Any route's resolve method can reject the promise with
   * { authRequired: true } to force a redirect. This method enforces that and also watches
   * for changes in auth status which might require us to navigate away from a path
   * that we can no longer view.
   */
  .run(function($rootScope, $location, ROUTES, loginRedirectPath) {

      // some of our routes may reject resolve promises with the special {authRequired: true} error
      // this redirects to the login page whenever that is encountered
      $rootScope.$on("$routeChangeError", function(e, next, prev, err) {
        if( angular.isObject(err) && err.authRequired ) {
          $location.path(loginRedirectPath);
        }
      });

      $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
        $rootScope.current_path = $location.path();
      });

      function check(user) {
        if( !user && authRequired($location.path()) ) {
          $location.path(loginRedirectPath);
        }
      }

      function authRequired(path) {
        return ROUTES.hasOwnProperty(path) && ROUTES[path].authRequired;
      }
    }
  );