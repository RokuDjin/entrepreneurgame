'use strict';

app.factory('Bank', function ($firebase, $rootScope, FIREBASE_URL) {

  var Bank = {

    // Set up the account for the first time
    init: function(uid) {
      var account = $firebase(new Firebase(FIREBASE_URL + 'checking/' + uid)).$asObject();
      account.$loaded(function () {
        if (account.$value === null) {
          account.name = "checking";
          account.transit = Math.floor(Math.random() * 9999) + 1000;
          account.number = Math.floor(Math.random() * 999999) + 100000;
          account.balance = 100;
          account.$priority = uid;
          account.$save();
        }
      });
    },

    // Put the checking account and its transactions into scope
    checking: function (uid) {
      var account = $firebase(new Firebase(FIREBASE_URL + 'checking/' + uid)).$asObject();
      account.$loaded(function () {
        $rootScope.checking = account;
      });
      var transactions = $firebase(new Firebase(FIREBASE_URL + 'transactions/' + uid + '/checking')).$asArray();
      transactions.$loaded(function () {
        $rootScope.checking_transactions = transactions;
      });
    },
  };


  return Bank;
});
