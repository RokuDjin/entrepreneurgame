'use strict';

app.factory('Helper', function ($firebase, $rootScope, FIREBASE_URL, Bank) {

  var Helper = {  

    // Helper that gets the difference between two dates. 
    monthDiff: function (d1, d2) {
      var months;
      months = (d2.getFullYear() - d1.getFullYear()) * 12;
      months -= d1.getMonth() + 1;
      months += d2.getMonth();
      return months <= 0 ? 0 : months;
    },

    // Method to return difference between two arrays of objects
    arrayDiff: function (array1, array2) {
      var array1Ids = {}
      array1.forEach(function(obj){
          array1Ids[obj.name] = obj;
      });

      return array2.filter(function(obj){
          return !(obj.name in array1Ids);
      });
    },

    // Returns the monthly income based on job type and hourly rate
    job_income: function(hourly, type) {
      if (type = "Full-Time") {
        return ((40*hourly)*52)/12;
      } else {
        return ((20*hourly)*52)/12;
      }
    },

    // Creates a random date between start and end
    randomDate: function(start, end) {
      return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    },

    // Given a daily schedule of user, it returns the total duration of all activities
    total_duration_helper: function (day) {
      var total_duration = 0;
      for (var i=0;i<day.length;i++) {
        total_duration += day[i].duration;
      }
      return total_duration;
    },

  };
  return Helper;
});