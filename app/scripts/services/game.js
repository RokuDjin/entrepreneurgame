'use strict';

app.factory('Game', function ($firebase, $rootScope, FIREBASE_URL, Bank, Helper, Assets) {

  var Game = {
    // Used to process games wins
    win: function ($scope) {
      var time = new Date().getTime();
      $scope.checking.balance += 20; // TODO change amount based on the game
      $scope.checking.$save();
      // add record of transaction
      $scope.checking_transactions.$add({amount: 20, description: "Won at Uldur ", time: time, priority: time, type: "deposit"});
      // increase the player's experience
      $scope.player.experience += 5; // TODO change based on current level and game played
      $scope.player.$save();
    },
    // Used to process games loses
    loss: function ($scope) {
      var time = new Date().getTime();
      $scope.checking.balance -= 20; // TODO change amount based on the game
      $scope.checking.$save();
      // add record of transaction
      $scope.checking_transactions.$add({amount: 20, description: "Lost at Uldur ", time: time, priority: time, type: "withdrawal"});
      // decrease player's health
      $scope.player.health -= 5; // TODO change based on current level and game played
      $scope.player.$save();
    },
  };

  return Game;
});

























