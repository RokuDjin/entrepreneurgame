'use strict';

app.factory('Player', function ($firebase, $rootScope, FIREBASE_URL, Bank) {
  var ref = new Firebase(FIREBASE_URL + 'players');

  var Player = {
    // Runs only once when a new user signed up
    create: function (uid) {
      var player = $firebase(ref.child(uid)).$asObject();
      return player.$loaded(function () {
        // Default Equipments
        player.hair = "images/hair/hair_1.png";
        player.head = "images/head/head_1.png";
        player.body = "images/body/body_1.png";

        player.rank = 1;
        player.level = 1;
        player.mana = 5;
        // attributes
        player.creativity = 1; 
        player.charm = 1;
        player.intelligence = 1;

        player.health = 100
        player.experience = 10;

        player.$save();

        // Creates the player bank account
        Bank.init(uid);
      });
    },
    // Puts the data needed globally in rootScope, for now the banking and player information
    setup: function(authUser) {
      var player = $firebase(ref.child(authUser.id)).$asObject();
      player.$loaded(function () {
        $rootScope.player = player;
      });
      Bank.checking(authUser.id);
    },
    // Adds an player attribute by using his available Mana
    add_attribute: function(player, attribute) {
      if (player.mana > 0) {
        player[attribute] += 1;
        player.mana -= 1;
        player.$save();
      }
    }
  };

  return Player;
});
