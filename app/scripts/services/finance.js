'use strict';

app.factory('Finance', function ($firebase, $rootScope, FIREBASE_URL, Bank, Helper, Assets) {

  var Finance = {

    // Used to put assets purchased and assets available for purchase in scope
    assets: function (uid, $scope) {
      // doing the same thing for purchased technologies and list of assets
      var purchased_assets = $firebase(new Firebase(FIREBASE_URL + 'assets/' + uid)).$asArray();
      purchased_assets.$loaded(function () {
        $scope.purchased_assets = purchased_assets; // This list will be used to add new items to purchased assets
        $scope.assets = Helper.arrayDiff(purchased_assets, Assets); // Items available for purchase
      });
    },

    // Used to make a purchase of any item and add it to list of purchased assets
    purchase: function ($scope, item, index) {
      var time = new Date().getTime();
      item.purchased = time;
      // Check if purchase is possible
      if ($scope.checking.balance >= item.cost) {
        // update the payment method balance
        $scope.checking.balance -= item.cost;
        $scope.checking.$save();
        // add record of transaction
        $scope.checking_transactions.$add({amount: item.cost, description: "Purchased " + item.name, time: time, priority: time, type: "withdrawal"});
        $scope.purchased_assets.$add(item);
        // remove the purchased item from view
        $scope.assets.splice(index, 1);
      }
    },
    // Sell an item and put money back into checking
    sell: function ($scope, item) {
      // remove the item from the purchased assets
      $scope.purchased_assets.$remove(item);
      // put money back to checking
      var value = this.value(item);
      $scope.checking.balance += value;
      $scope.checking.$save();
      var time = new Date().getTime();
      $scope.checking_transactions.$add({amount: item.cost, description: "Sold " + item.name, time: time, priority: time, type: "deposit"});
    },
    // A method that calculates the current value of an asset based on when it was purchased and depreciation
    value: function (item) {
      var months_since_purchase = Helper.monthDiff(new Date(), new Date(item.purchased));
      return (item.cost - ( (months_since_purchase*item.depreciation)*item.cost/100 ));
    },
  };

  return Finance;
});

























