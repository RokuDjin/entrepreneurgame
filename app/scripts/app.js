/* global app:true */ 
'use strict';

/**
 * @ngdoc overview
 * @name entrepreneurgameApp
 * @description
 * # entrepreneurgameApp
 *
 * Main module of the application.
 */

var app = angular
  .module('entrepreneurgameapp', [
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'ngDragDrop'
  ]);


