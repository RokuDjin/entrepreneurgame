'use strict';

app.controller('ScheduleCtrl', function ($scope, $rootScope, $location, user, Player, Activity, Schedule) {
	Player.setup(user); // Puts needed data into rootscope

  Schedule.get(user.id, $scope);

  Activity.set($scope);

  $scope.startCallback = function(event, ui, activity) {
    $scope.activity = activity;
  };

	$scope.dropCallback = function(event, ui, schedule) {
    Schedule.add(schedule, $scope.activity);
  };
  $scope.total_duration = function() {
  	return Schedule.total_duration($scope.schedule);
  };
  $scope.remove = function (schedule, activity) {
    schedule.$remove(activity);
  }
  
});