'use strict';

app.controller('ShoppingCtrl', function ($scope, user, Player, Finance) {
	Player.setup(user); // Puts needed data into rootScope

	Finance.assets(user.id, $scope);

  $scope.buy = function (item, index) {
    Finance.purchase($scope, item, index);
  }


});