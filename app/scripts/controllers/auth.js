  'use strict';

app.controller('AuthCtrl',
  function ($scope, $location, Auth, Player) {

    $scope.login = function () {
      Auth.register($scope.user).then(function (authUser) {
        // Initialize data
        Player.create(authUser.id);
        Auth.login($scope.user).then(function () {
          $location.path('/universe');
        });
      }, function (error) {
        if (error.code == "EMAIL_TAKEN") {
          Auth.login($scope.user).then(function () {
            $location.path('/universe');
          }, function (error) {
            $scope.error = error.toString();
          });
        }
      });
    };
  });
