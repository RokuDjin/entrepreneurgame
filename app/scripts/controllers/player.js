'use strict';

app.controller('PlayerCtrl', function ($scope, $rootScope, user, Player) {
	Player.setup(user); // Puts needed data into rootscope

	$scope.add = function(attribute) {
		Player.add_attribute($rootScope.player, attribute);
	};

	$scope.change_head = function(number) {
		$scope.player.head = "images/head/head_"+number+".png";
		$scope.player.$save();
	};

	$scope.change_body = function(number) {
		$scope.player.body = "images/body/body_"+number+".png";
		$scope.player.$save();
	};

	$scope.change_hair = function(number) {
		$scope.player.hair = "images/hair/hair_"+number+".png";
		$scope.player.$save();
	};

});
