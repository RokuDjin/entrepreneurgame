'use strict';

app.controller('HomeCtrl', function ($scope, $location, user, Player, Finance, Bank) {
	Player.setup(user); // Puts needed data into rootscope

	Finance.assets(user.id, $scope);

	$scope.sell = function(item) {
		Finance.sell($scope, item);
	};

	$scope.value = function(item) {
		return Finance.value(item);
	};

});