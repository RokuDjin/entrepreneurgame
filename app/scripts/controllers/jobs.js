'use strict';

app.controller('JobsCtrl', function ($scope, $rootScope, user, Player, Finance) {
	Player.setup(user); // Puts needed data into rootscope

	Finance.jobs(user.id, $scope);

	$scope.resign = function (job) {
		$scope.current_jobs.$remove(job);
	};

	$scope.apply = function (item, index) {
    // $scope.pending.$add(item);
    // TODO if can get the job, add it to list of income
    $scope.jobs.splice(index, 1);
  };

  $scope.chance = function (item) {
    var player = $rootScope.player;
    if (player != undefined) {
      if ( (item.intelligence > player.intelligence) || (item.charm > player.charm) || (item.creativity > player.creativity)) {
        return "red";
      } else {
        return "green";
      }
    }
  };

});
