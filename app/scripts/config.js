  // version of this seed app is compatible with angularFire 0.6
  // see tags for other versions: https://github.com/firebase/angularFire-seed/tags
  app.constant('version', '1.0.0');

  // where to redirect users if they need to authenticate (see routeSecurity.js)
  app.constant('loginRedirectPath', '/');

  // your Firebase data URL goes here, no trailing slash
  app.constant('FIREBASE_URL', 'https://uthon.firebaseio.com/');
