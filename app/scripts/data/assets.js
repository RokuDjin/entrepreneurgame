'use strict';

app.factory('Assets', function () {

  var Assets = [
    { 
      name: "Computer",
      cost: 400,
      stars: 1,
      depreciation: 10,
      provides: ["Computer"]
    },
    { 
      name: "Phone",
      cost: 100,
      stars: 1,
      depreciation: 15,
      provides: ["Phone"]
    },
    { 
      name: "Car",
      cost: 1000,
      stars: 15,
      depreciation: 10,
      provides: ["Car"]
    },
    { 
      name: "Console",
      cost: 500,
      stars: 1,
      depreciation: 8,
      provides: ["Video Games"]
    },
    { 
      name: "Television",
      cost: 1000,
      stars: 5,
      depreciation: 5,
      provides: ["Television"]
    },
  ];

  return Assets;
});
