'use strict';

app.filter('age', function () {
  return function (birthday) {
  	birthday = new Date(birthday);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // milliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };
});

app.filter('duration', function () {
  return function (duration) {
  	if (duration < 60) {
			return Math.round(duration/60*10)/10;
		} else {
			return (Math.floor(duration/60) + "." + Math.floor(duration%60*100/60).toString()[0]);
		}
  };
});

app.filter('value', function () {
  return function (value) {
    if (value > 0) {
      return "+" + value;
    } else {
      return value;
    }
  };
});

app.filter('customCurrency', ["$filter", function ($filter) {       
    return function(amount, currencySymbol){
        var currency = $filter('currency');         

        if(amount < 0){
            return currency(amount, currencySymbol).replace("(", "-").replace(")", ""); 
        }

        return currency(amount, currencySymbol);
    };
}]);